* Name: Character Fragment Attribute (SGR codes 110/111)
* Start Date: 2020-01-31
* [It's not finished yet]

# Summary

The escape sequence to set a clustering (tiling?) mode for subsequent text into grapheme blocks (tiles?) of a specified size or select any fragment of the block.

How should we call it, _tile_? _ligature_? ...?

```
       ┌──────────── ligature (tile?) width on the screen, range: 1..16, cells
       │ ┌────────── ligature (tile?) height on the screen, range: 1..4, cells
       │ │ ┌──────── column selector, range: 1..W, 0 - select all, cells
       │ │ │ ┌────── row selector, range: 1..H, 0 - select all, cells
       │ │ │ │ ┌──── text cluster size, codepoints, range: >0
       │ │ │ │ │ ┌── rotation of the text inside the ligature (tile?): 0 - 0°, 1 - 90°, 2 - 180°, 3 - 270°
\e[110:W:H:X:Y:S:Rm
```

 - `W`, `H`: missing numbers are treated as 1.
 - `X`, `Y`, `S`, `R`: missing numbers are treated as 0.
 - If any of the `X`, `Y`, `S` are specified, the sequence 
   should be applied only to the following text cluster, and 
   the mode should not be changed.
 - If any of the `X` or` Y` is non-zero, then the sequence represents the fragment at the intersection of the selected column and row, and not the ligature (tile?) itself.
 - The SGR Reset `\e[m` resets the character fragment attribute.

...

...define attribute itself

...

The new SGR 110[:...] (SGR 111[:...]) attribute, with a colon as parameters separator (colon-separated sub-parameters...), is introduced to explicitly set the display size of a ~~character~~ (cluster?) or select any fragment of it.

...

A new SGR attribute is proposed that allows terminals to annotate each cell with fragment (tiling?) metadata. The renderer takes this metadata and displays in the cell either the entire ~~character~~ (cluster?) or its fragment. By using this attribute, applications are able to ( to control (?)) explicitly set the display size of a ~~character~~ (cluster?) or select any fragment of it. ... text run clusterization control ? ...

...

...

grapheme cluster ⬌ cell matrix (1×1..4×16)

# Motivation

Dealing with characters of various widths is sometimes associated with problems such as:

- there is no way to specify (to control a width) a custom width for the character when necessary
  - there is no way to simultaneously display the same characters, both narrow and wide variants
  - there is no way to use triple and quadruple characters along with narrow and wide
  - display private use area characters in variants specified by the application
  - ... orthographic ligatures, devanagari syllables, etc
  - ... ZWJ ...
- dependence on a specific Unicode Standard (not font or shaping model)
  - a different interpretation of character width in applications and terminal emulators (talking about future)
  - ambiguous cursor position calculated by the application during atomic block formation
- there is no way to display wide characters partially
  - halves of wide characters cannot have different colors
  - ... placing the application cursor (visual clue) inside ligatures (box, underline)... cursor positioning in the middle of a wide character
- no characters higher one cell
- no characters of the subcell size
- ... text run clusterization (ligation) control ...

# Detail

#### Definitions


...

- _Character_ — user-perceived character, grapheme cluster, conjuncts and any dependent combining characters (conjuncts usually contain multiple grapheme clusters)
- _Simplex_ — whole _character_ that occupies only one cell / ... Nx = Dx = Ny = Dy = 1
- _Compound_ — _character_ that occupies more than one cell
  - _Solid_ — _compound_ that represents a whole _character_ / ...(Nx > Dx) and (Ny > Dy)
  - _Slice_ — _compound_ that represents a single horizontal or vertical line of character cells / ...horizontal: ((Nx > Dx) and (Ny ≤ Dy)) ... or vertical: ((Nx ≤ Dx) and (Ny > Dy))
- _Fragment_ — specific cell (specified by Nx and Ny) of _compound_ / ...(Nx ≤ Dx) and (Ny ≤ Dy)
- _Fraction_ — _fragment_ or _slice_ ... (?)
- _Volume_ - _character_'s memory buffer length expressed in code points


#### Syntax

The character fragment attribute is an 16-bit integer and is used to set the size of solid _character_ up to 4×16 cells or select any _fraction_ of it. The last four bits are to specify the following:

- R: 2 bits - rotation of the text inside the ligature: 0 - 0°, 1 - 90°, 2 - 180°, 3 - 270°
- C: 1 bit - a flag showing how the cluster is selected from the text run - either using the shaping model, or explicitly specified
- 1 bit reserved

The attribute can be parameterised by five integers:
- `Nx`, `Dx` - are from 1 to 16
- `Ny`, `Dy` - are from 1 to 4
- `R` - from 0 to 3
- `C` - 0 or 1
- `Sz` - the integer to specify the _volume_

where
- N[x,y] represents either the width of the whole character or it is a segment selector of the available D[x,y] parts along the [X,Y]-axis;
- D[x,y] represents either 1 or it is the number of parts (ligature size) along the [X,Y]-axis in case of the N[x,y] is the segment selector.
- Sz represents grapheme cluster length (codepoints count). Zero - the cluster len should be determined by using a shaping model.

The following combinations of values are meaningful (separate interpretation for each axis)
- N ≤ D - select the part N of the character from the available D parts and use it as a single-cell fraction of the character (along the corresponding axis);
- N > D - stretch the character to N cells.

_rework_
The attribute has the following states:
- _disabled_, the terminal is responsible for the _characters_ size (auto size, default)
- _enabled_, the terminal uses the specified size for each _character_ in the subsequent text
- _disabled_ and _volume_ is specifed, the terminal is responsible for the _characters_ size (auto size, default), but the grapheme cluster volume is specified by the application
- _enabled_ and _volume_ is specifed, ... , but the grapheme cluster volume is specified by the application

The attribute can be enabled using five parameters or directly by value, either can be disabled:


//todo add R and C
Normal:
- Parameterised: `CSI` `110` [ `:Pn` ] `m`
- By value: `CSI` `111` [ `:P` ] `m`
  - Pn = `Nx:Dx:Ny:Dy:Sz` , where Nx, Dx, Ny, Dy ∈ [1‥8], Sz ∈ [1‥16]
  - ~~P = (Nx - 1) + (Dx - 1)×16 + (Ny - 1)×256 + (Dy - 1)×1024 + (Sz - 1)x ... ~~

Disabled: 
- `CSI` `110:0` `m`

_Note_: 
- SGR code **110**: missing numbers in `Nx:Dx:Ny:Dy` are treated as 1, missing `Sz` as 0
- SGR code **111**: missing number is treated as 0
- SGR Reset `ESC[m` resets the character size attribute to _simplex_ (1×1) if it is _enabled_
- In case the `Sz` is specified, the attribute should be applied to only the following cluster, and the mode should not be changed

#### Behavior

- The terminal never stores _compounds_, only _fragments_.
- The terminal always breaks _compounds_ into a matrix of _fragments_.
- The terminal snaps _compounds_ to the baseline at the bottom (character face bottom edge baseline).
- If the _compound_ does not fit at the end of the current line, it must be placed to a new line with the addition of empty lines in accordance with its height.
- The cursor height always matches the size of the _character_ matrix behind (cursor at bottom of the character, base line).
- The cursor step always matches the width of the adjacent _character_ matrix (... base line (?)). If the _character_ matrix is not a whole _character_, the step is equal to one.
- Backspace/Delete: if there is a whole (matrix ... base line) _compound_ behind/ahead of the cursor, then it must be replaced by spaces (or other fallback characters), otherwise only the one cell must be cleared (? Delete: del a character, Backspace del a single codepoint).
- On copy-pasting, just copy all _fragments_ and, if possible, combine adjacent halves into one.
- _Compounds_ are always drawn as a matrix over the grid of cells, the cursor moves forward in accordance with their width.
- Regardless of the number of cells occupied by the _character_, glyph proportions must be preserved by placing inside.
- On rewrap-on-resize, _compounds_ (because matrix ...) that are higher than one cell can be destroyed, like any pseudo-graphic drawing,  when vertical breaks and transfer of each slice separately are inevitable.
- If an application crashes and does not reset, the user can still issue a reset command in the shell to restore normal rendition.
 
...

- ECH	Erase character
- DCH	Delete character
- ICH	Insert character
- DL	Delete line
- EL	Erase in line
- IL	Insert Line
- ED	Erase in display

...

#### Implementation

...

The attribute occopies 16 bits of space in the cell attribute structure.

...

Implementing font exchange between the application and the terminal

Implementing text run clusterization (use shaping engine)

Support the escape sequence, enable storing it in the buffer and then support drawing it using the renderers.  

+behavior

#### Examples

- `ESC[110:0m` - set to auto/disabled
- `ESC[110mA`
  - ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/SGR-CFA-A.png) - 1×1 narrow
- `ESC[111mA`
  - ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/SGR-CFA-A.png) - 1×1 narrow
- `ESC[110:1mA`
  - ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/SGR-CFA-A.png) - 1×1 narrow
- `ESC[110:2m是`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/dc9fbdb21c93fe582851b2df3db4d0d1/image.png) - 2×1 wide
- `ESC[110:3mच्छ्य`
  - ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/SGR-CFA-Indic.png) - 3×1 triple
- `ESC[110:3m👨‍👩‍👧‍👦`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/ae25b8ff224676d8ff44ae8a82346396/image.png) - 3×1 triple
- `ESC[110:2:3m👨‍👩‍👧‍👦`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/9aeaa973ae69a8e554de179c9b9c87ea/image.png) - 1×1 middle of triple
- `ESC[110:1:2m😊`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/acf01542b9f4408477794422c65fcffb/image.png) - 1×1 left half of wide
- `ESC[110:2:2m😊`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/d41460f0be0975fe71d38564910397b8/image.png) - 1×1 right half of wide
- `ESC[110:2:1:2:1mE`
  - ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/SGR-CFA-E.png) - 2×2 H3
- `ESC[110:2:1:1:2mE`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/a630bfae189580e5f94027f74b7c8644/image.png) - 2×1 top half of 2×2
- `ESC[110:2:1:2:2mE`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/021fd4a6bb6be3e7ccfb7243df8e41d9/image.png) - 2×1 bottom half of 2×2
- `ESC[110:1:2:1:2mE`
  - ![image](https://gitlab.freedesktop.org/o-sdn-o/specifications/-/wikis/uploads/5c0d0f0b3eda8180473c485e67212364/image.png) - 1×1 top-left corner of 2×2

