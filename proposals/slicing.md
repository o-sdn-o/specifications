# Character Scaling in a Fixed-pitch Environment
Date: 2020-01-31  
[It's not finished yet]

### Table of Contents <a name="toc"></a>

- [1 Introduction](#Introduction)
   - [1.1 Overview](#Overview)
   - [1.2 Discussion](#Discussion)
     - [EXPERIMENTAL: "Set Character Width proposal (version 3) by Markus Kuhn"](#Kuhn)
     - [iTerm2: "Console confused by emojis with variant selector 0xFE0F"](#iTerm2)
     - [Term WG: "Double-width characters in Unicode 9+"](#Double)
     - [GNOME/VTE: "DECDWL/DECDHL (double width/height) support"](#DECDWL)
     - [Term WG: "variable-width/-height characters and glyphs"](#variable)
     - [Windows Terminal: "Incorrect display of characters written on top of the wide emojis #4345"](#Incorrect)
   - [1.3 Goals](#Goals)
   - [1.4 Definitions](#Definitions)
- [2 Design Guidelines](#Design)
  - [2.1 General Approach](#Approach)
  - [2.2 Concept](#Concept)
    - [2.2.1 Mathematical Model](#Mathematical)
    - [2.2.2 Representation](#Representation)
      - [Packing](#Packing)
      - [Normalization](#Normalization)
      - [Decomposition](#Decomposition)
      - [String View](#String)
      - [Scrollback Buffer](#Scrollback)
      - [Screen Buffer / Plain Text File](#Screen)
  - [2.3 Names](#Names)
- [3 Expected Behavior](#Expected)
  - [3.1 Printing](#Printing)
    - [Line Wrap](#Wrap)
    - [BiDi (LTR, RTL, TTB)](#BiDi)
    - [Side effects](#Side)
  - [3.2 Capturing](#Capturing)
  - [3.3 Collating](#Collating)
  - [3.4 Canonical Equivalence](#Canonical)
- [4 Compatibility](#Compatibility)
  - [4.1 Terminal Emulators](#Emulators)
  - [4.2 Console Text Editors](#Editors)
  - [4.3 Terminal Multiplexers](#Multiplexers)
  - [4.4 POLA](#POLA)
- [5 Unicode Standard](#Unicode)
  - [Prefix or Suffix](#PrefixSuffix)
  - [5.1 Suffix](#Suffix)
    - [Variation Sequence](#VS)
	- [Placement in the Text](#Placement)
  - [5.2 Prefix](#Prefix)
    - [slice p along X-axis](#sliceX)
    - [slice q along Y-axis](#sliceY)
    - [fraction](#fraction)
    - [character](#character)
    - [embedding scaled text](#scaledtext)
  - [5.3 Security Issues](#Security)
    - [Unicode Security Considerations](#Considerations)

## 1 Introduction <a name="Introduction"></a>

Current practice is limited only by wide and narrow characters. Moreover, there is no way to specify the desired size, as well as process any halves of characters. Using other sizes and processing of individual halves requires a generalization of this approach.

The terminal emulators provide a monospaced _W×H_ character grid. To independently change any cell, even if such a cell is part of a character, an attribute indicating the parts must be in each cell. The attribute must be a scalar value that uniquely determines the size of the character and its selected fragment.

The internal structure of the attribute should allow each scaled character to be represented in the following forms:
- single scaled element
- horizontal(vertical) sequence of vertical(horizontal) slices
- matrix of individual cells

Examples of using scaled characters:
- Partial destruction of a wide character (2×1 cells)
  > ![image](https://dice.netxs.online/index.php/apps/sharingpath/sdn/character-fragment-attribute/expected-output-fractaling.png)
- Text ... (2×2 cells) and ... (1×1 cell)
  > ...
- Text ... (4×1 cells)
  > ...
- Text ... (3×2 cells)
  > ...

[ToC](#toc)

## 1.1 Overview <a name="Overview"></a>

general structure of the document, all points in order...
- discussion
- design
- goals
- definitions
- concept
- model
- representations
- naming
- behavior
- compatibility
- further standardization

[ToC](#toc)

### 1.2 Discussion <a name="Discussion"></a>

[Set Character Width proposal (version 3) by Markus Kuhn](https://www.cl.cam.ac.uk/~mgk25/ucs/scw-proposal.html) <a name="Kuhn"></a>

<details><summary>click to expand excerpts...</summary><p>	

... see link ...

[ToC](#toc)
</p></details> 

[iTerm2: "Console confused by emojis with variant selector 0xFE0F"](https://gitlab.com/gnachman/iterm2/issues/7239) <a name="iTerm2"></a>

<details><summary>click to expand excerpts...</summary><p>	

- @clockworksaint:
  > Emojis composed with the "variant selector 16" character code appear as the wrong width and confuse the backspace motion
- @gnachman:
  > VS16 can turn the character wide, for example <1, VS16> makes the normally narrow "1" into a wide character. Luckily, the other direction does NOT happen; a wide character (which includes all characters which have emoji presentation) followed by VS15 still is wide  
  > ... most programs use wcwidth at the current time so I'm disinclined to do something fancy for VS16 which would add a bunch of complexity and break existing applications. I expect this will change over time, though.

[ToC](#toc)
</p></details> 

[Term WG: "Double-width characters in Unicode 9+"](https://gitlab.freedesktop.org/terminal-wg/specifications/issues/9) <a name="Double"></a>

<details><summary>click to expand excerpts...</summary><p>	

- @gnachman:
  > ... VS16 (U+FE0F) makes the character it combines with double-width, wcwidth() is incapable of dealing with this properly because it only looks at the base character, almost all applications won't handle grapheme clusters with this codepoint correctly  
  > ... the additional wrinkle that all Emoji are treated as double-width in Unicode 9+, even if east-asian-width does not call for it  
  > ...  it would be helpful to come to an agreement on how best to compute character width ...  
  > ... I think the hard part is getting apps & terminal emulators in sync, whatever algorithm we end up choosing.  
  > ... IMO we need to sort out [#7](https://gitlab.freedesktop.org/terminal-wg/specifications/issues/7) / [#8](https://gitlab.freedesktop.org/terminal-wg/specifications/issues/8) before it makes any sense to invest in this issue.

- @textshell :
  > Requiring all implementations to use a common library will not work  
  > ... we need a single point of truth for the width ...

- @kovidgoyal:
  > ... as for using communication for this, that is an absolute no-no. It's slow and inefficient as hell ...

- @textshell :
  > There is no way to calculate the width of a arbitrary string independent of display properties any more (there are at least a dozen different width mappings in use, and even for one terminal there are different unicode versions that might or might not be supported).  
  > ... for applications like tmux or screen in split mode this (application to announce what width-table/algorithm it's using) might prove to be really impossible because they can only request one width-table from their upstream terminal

- @egmontkob:
  > ... What's the largest unit where we cannot rely on wcswidth() being the sum of wcwidth()s? Silly example, but let's say wcswidth() figures out that "maf" is 3 wide, "ia" is 2, but "mafia" is 4 because an "fi" ligature should be used. There's no way any terminal emulator can handle it properly, especially if the letters arrive one by one, or the cursor is crazily positioned in between emitting these letters.  
  > ... example is Lam+Alef in Arabic, which is mandatory to be rendered as a ligature, preferably with a width of one, which is practically unsolveable within the realm of terminal emulation...  
  > ... the largest scope where wcswidth() should be allowed to diverge from the sum of wcwidth()s is a base letters + modifiers (e.g. combining accents, VS16 etc.)  
  > ... Most apps, including ncurses based ones, and presumably terminal emulators too go with wcwidth()/wcswidth() or something similar because that's what they have access to without having to manually duplicate a Unicode table and keep up to date with it.  
  > ... if we come up with a recommendation for the default behavior that differs from the current practice (which is to eventually follow Unicode), since it'll be adopted by a subset of the ecosystem, it'll cause more troubles than it'll solve.  
  > ... You can mitigate the problems of choose to follow Unicode by introducing an escape sequence that switches to a particular Unicode version, and perhaps even placing that sequence in your text files. ...  

- @mintty:  
  > \> @kovidgoyal: The system wcswidth() lags the unicode standard by years...  
  The point is that system administrators are often reluctant to apply such updates, especially in industrial environments.  
  > ... (95% of the characters in the UCD will never change their widths) That's not a valid conclusion if you consider e.g. a text editor as a use case. If the width of one character in a line is wrong, all subsequent characters on that line will be misplaced...  

- @kovidgoyal:
  > ... on macOS wcwidth() was, last time I checked based on Unicode 5...  
  > ... servers running very old/stable distros that dont update glibc and friends...  
  >  ... a single character having wrong width messes up the rest of the line, in editors ... the terminal and editor are basing their width calculations on a different unicode standard, even then it is highly unlikely they will encounter any such characters...  

- @egmontkob:
  > ... who should be able to adjust its behavior? There are a few dozens of terminal emulators out there, and hundreds (if not thousands) of apps. Many of these apps rely on a library like ncurses which I find unlikely to diverge from wcwidth.  
  > ... applications should only know the version they're using...  

- @kovidgoyal:
  > in my experience, simply using up-to-date unicode standards based wcswidth() works fine in practice...  
  > How will changing widths in the terminal work? ... What happens if an application changes the width to X but the next application expects Y?  

- @egmontkob:
  > ...it would be each application's job to clean up...  

- @@gnachman:
  > Almost all apps will continue to use wcwidth. If we provide some mechanism for reporting and setting Unicode version, the benefit will be small, and it will create new problems (**what if vim sets the unicode version and then crashes?** etc.)  
  > ... some users have issues with Telugu, which seems like it ought to be double-width but its characters are listed as narrow in east asian widths ...

- @mintty:
  > The single/double cell width paradigm is well suited for CJK but Indic scripts and a few others (including some Arabic ligatures) are simply beyond that mechanism with their variable width. A terminal can hardly handle them.

[ToC](#toc)
</p></details> 

[GNOME/VTE: "DECDWL/DECDHL (double width/height) support"](https://gitlab.gnome.org/GNOME/vte/issues/195) <a name="DECDWL"></a>

<details><summary>click to expand excerpts...</summary><p>	

- @rbanffy:
  > ... a lot of people use things like Terminator and tiling window managers with plain terminal windows. Those people may be able to benefit from DECDWL/DECDHL.  
- @egmontkob:
  > Double width/height character support, on a per-line basis, cannot be implemented without breaking something existing, in fact, multiple existing features: the ability to print text that just overflows to the next line wherever the margin is reached, the ability to reflow text on resize, the ability to tmux anything into vertical splits  
  > ... If there's a need for double-height or double-width (as in stretched) characters, it needs to go per-character

[ToC](#toc)
</p></details> 

[Term WG: "variable-width/-height characters and glyphs"](https://gitlab.freedesktop.org/terminal-wg/specifications/issues/21) <a name="variable"></a>

<details><summary>click to expand excerpts...</summary><p>	

- @Per_Bothner:
  > ... how to extend the terminal model to handle variable-sized characters ...  
  > ... The main thing missing is to specify escape sequences to make it easier for an application to benefit from this model...  
  > ... dealing with variable-sized characters dealing with images and graphics is more natural...  

- @jerch:
  > ... the problem here (as with anything that deals with more advanced aspects of Unicode) - app side and terminal side have to agree on the rules...  
  > ... We certainly lack some Unicode handshaking and support in low level libs in general (like the tty does not know anything about Unicode beside a rather wicked UTF8 accounting, or libreadline would have to support it with same unicode version/rules). Then there is ssh - lol...

[ToC](#toc)
</p></details> 

[Windows Terminal: "Incorrect display of characters written on top of the wide emojis #4345"](https://github.com/microsoft/terminal/issues/4345#) <a name="Incorrect"></a>

<details><summary>click to expand excerpts...</summary><p>	

- @o-sdn-o:
  > wide characters have to be completely destroyed when another symbol hits it, but expected behavior is to show wide character halves (to correctly overlap text fragents or wrap text line that contains wide chars)  
- @DHowett-MSFT:
  > Emoji cannot be split in half. ... There is no way for us to properly cut an emoji, or a CJK symbol that spans two cells, in half. ... It does not seem trivial as a human to (mentally) reconstruct a symbol from an ideographic language split and moved to the other side of the screen.  
- @egmontkob
  > when the cursor is at the rightmost column (i.e. there's still room for a final single character in the row) and a double wide is printed, the new double-wide character is placed entirely in the next line, leaving an empty cell at the end of the previous ... this is one of the reasons VTE forces a minimum size of 1 row, 2 columns.  
... partial overwriting approach is problematic for multiple reasons.  
... (halves) is a natural requirement for a text editor with non-folding lines and horizontal scrolling  
... If there's a need to display half-emojis, a new escape sequence should be invented which allows to place a half-emoji anywhere, without affecting any other cell. ... new mode to SAPV, or some brand new sequence  
... what to do (with halves) on a copy-paste?  
... what to do on rewrap-on-resize, how to decide whether an emoji (or a matching pair of two half-emojis) are allowed to be separated to different lines or not?  
... If Unicode adds it as a suffix character (VARIANT SELECTOR, like VS16), it hardcodes that terminals won't be able to support it  
... Unicode's width model is already incompatible with what terminals and terminal-based applications expect  
... introduces another surface for homoglyph attacks  
... some Unicode modifiers increase the width of a character in a way that it can be 4 or even more cells wide. Yet another sign of Unicode not caring about terminals, and terminals having no idea how to follow the specs they come up with  
... And if we allow these (i.e. units that occupy more than 2 cells horizontally), you'll need a solution for displaying any of its cells and only those, e.g. third or fourth or three fourths of such a grapheme...  
- @jerch:
  > - grapheme clustering: currently not handled correctly by any common terminal or cmdline lib - this should be changed  
  > - wcwidth is kinda broken / cannot deal with complicated clustering rules anymore  
  > - different screen rendering possibilities given by unicode (like emojis can have a text/picogram repr, compounds can have different "run-widths") needs to be dealt with  
  > - different unicode versions imply different rules, needs some kind of version "handshaking"

[ToC](#toc)
</p></details> 

[ToC](#toc)

### 1.3 Goals <a name="Goals"></a>

problems:
- proper operation of screen drawing libraries and multiplexing applications (no halves of wide characters)
- different interpretation of character widths in terminals and applications
- the provider of information presented in plain text cannot be sure that it is displayed correctly at higher levels of presentation, as well as in the integrity of its replication
- formatting headings in documents presented in plain text without any extrernal text-decoration and markup applications (absence of typographical features expressed in plain text)
- lack of typography presentation of simple mathematical formulas in documents presented in the form of simple text, as well as superscript and subscript text
- impossibility of multiplexing of text containing characters of different widths (CJK, Emoji, e.t.c), documents side to side, as well as showing polycolored background of these wide characters in a cell-based grid environment
- the absence of any agreements on the size of characters between the providers of information presented in the form of plain text and its consumers
- regular introduction of new characters with new sizes and ways to display them
- wcwidth implementations are locale dependent
- the dependence of the presentation of characters on the context of their presence, in some cases the characters should be considered wide, in others narrow
- Unix like aproach is the plain text, ...for laying out tabulated data in plain text documents
- this needs to support terminals that are simple like the linux kernel console (kmscon) (you really want you editor to work when recovering your system).
- support for folding of the text within wide characters
- support for leading (typography)  
e.g., line of characters in 1×2 cells will be centered vertically in these cells, which will lead to an increase in line spacing, a similar increase in line spacing will occur for larger sizes 1×3 and 1×4.
- source code: monospaced
- independence from changes of the Unicode Standard.
- there is absolutely no guarantee that the exact same plain text file will be printed exactly the same after N years
- lines with different font heights very useful when reading logs containing lines with different relevance
- performance ... rendering text as a cell of grids

- @kovidgoyal ([kitty](https://github.com/kovidgoyal/kitty/issues/1978#issuecomment-571514368)) ... Rendering works by mapping each cell to a number. That number acts as an index into a sprite map which is uploaded to the GPU. The list of numbers is sent to the GPU and the GPU renders them in a single pass. In the case of ligatures, the **entire ligature is rendered, then split into cells and the cells added to the sprite map**.
- @kovidgoyal [kitty](https://github.com/kovidgoyal/kitty/issues/1978#issuecomment-575430679) ... Pretty much all advanced terminal applications use their own wcswidth() implementations, precisely because glibc's is a broken umm POS. glibc is not the canonical source for how to calculate widths, the unicode standard is. And kitty's wc(s)width is autogenerated from the unicode standard. Indeed using the system libc's wcwidth() is fundamentally a bad idea because it can be arbitrarily old and broken. Not to mention it can vary between systems when you ssh. Any serious terminal application needs to use a standards based implementation.

This document provides a general description of the solution to the problem of representing multisized (up to 4×4 cells) characters and their fractions in a cell-based grid, where each cell one-to-one represents the visible part (fraction) of the character, as well as representing heterogeneous text as an array of strings composed of multisized characters.

The following applications are related to multisize characters and character segmentation:
- Terminal Emulators;
- Linux console ([kmscon](https://www.freedesktop.org/wiki/Software/kmscon/))
  - framebuffer implementation of text Linux console that doesn't use hardware text mode (useful when that mode is unavailable, or to overcome its restrictions on glyph size, number of code points etc.;
  - describes the content of a text screen in terms of code points (has a very limited 512 characters font, but does support the unicode capability) and character attributes and not in terms of grapheme clusters yet;
- Windows Console;
- Plain text documents.

The main idea is to allow the application to disable the terminal’s (or other environment) participation in calculating character widths by allowing the application to set the default size for characters by itself, as well as the ability to set specific sizes for individual characters by emitting the corresponding commands.

The solution described in the document allows to manipulate, store and display multisized characters in a cell-based grid, as well as their individual fractions and their vertical and horizontal splits.  
The solution also solves the problem of displaying wide characters in terminals by letting ~~the terminal or~~ the application running in it decide how wide the character will be, rather than relying on external data sources of these values that are subject to regular changes. The goal is being able to output a text file known display characteristics with characters lined up in columns and have output work with that text file everywhere and it's guaranteed to be stable (the exact same text file will be printed exactly the same way in N years from now).

[ToC](#toc)

### 1.4 Definitions <a name="Definitions"></a>

- _Character_ — a user-perceived character or a grapheme cluster
- _Simplex_ — a representation of a _character_ that occupies only one cell
- _Compound_ — a representation of a _character_ that occupies more than one cell
  - _Solid_ — a _compound_ that represents an entire _character_
  - _Strip_ — a _compound_ that represents a single horizontal or vertical line of character cells
- _Fragment_ — a cell element of the _compound_
- _Fraction_ — ... a _fragment_ or a _strip_ ...

...

***character*** — [Grapheme cluster, user-perceived character](https://unicode.org/reports/tr29/#Grapheme_Cluster_Boundaries).  
***solid*** — Solid _character_ with a size of _n×m_ tiles.  
***strip*** — Single column or row of tiles of a _solid_.  
***piece*** — Single tile of a _solid_ or _strip_.  

[ToC](#toc)

## 2 Design Guidelines <a name="Design"></a>

### 2.1 General Approach <a name="Approach"></a>

- a command that explicitly sets the size of subsequent characters
  - a command to reset / cancel this size to default (fixed size or environment decisions).

these commands should work independently.

The COMMAND(_n×m_) that turns the terminal in a fixed-size _n×m_ character scaling mode (global scaling state: terminal treats all characters emitted by app as _n×m_) and snap to the baseline for the **bottom** of the character
- _n,m ∈ [1, 4]_ - fixed char size _n×m_

The COMMAND(_Nx_, _Ny_, _Dx_, _Dy_) that explicitly sets the size of subsequent characters or reset current size to default (fixed size or environment decisions).

1. Scaling mode is off by default - terminal emulator treats all chars as he wants, use own _cwidth_ algorithms or whatever.
2. Global Scaling State - fixed scaling mode which introduced by the application knows what width of characters should be - when global scaling mode was introduced by the application, all characters are treaded by the terminal as a specified size until no specific params was introduced (rendition state). E.g., application emits that all characters should be treaded as _1×1_, hence treminal prints all as _1×1_ until this mode turned off by the application.
3. Local Scale Params (rendition state) specified by the app by emitting special rendition command. Resetting the rendition params also resets the Local Scale Params to Global Scaling State.

[ToC](#toc)

### 2.2 Concept <a name="Concept"></a>

#### 2.2.1 Mathematical Model <a name="Mathematical"></a>

Consider a set of characters with size not exceeding _4×4_ tiles. To correctly display of either the entire character from the set, or any rectangular fraction of it, four numeric parameters are required with values from 1 to 4 ... 

...

assign two pairs of numbers describes _fraction_ and _slice_

...

_Ps_ = { _Nx_, _Dx_, _Ny_, _Dy_ }

where
- _N[x,y]_ represents either the width of the whole character or it is a segment selector of the available _D[x,y]_ parts along the _[X,Y]-axis_;
- _D[x,y]_ represents either equals 1 or it is the number of parts along the _[X,Y]-axis_ in case of the _N[x,y]_ is the segment selector.

The following combinations of values are meaningful (separate interpretation for each axis)
- _N ≤ D_ - select the _N_ part of the character from the available _D_ parts and use it as a single-cell fraction of the character (along the corresponding axis);
- _N > D_ - stretch the character to _N_ cells.

[ToC](#toc)

#### 2.2.2 Representation <a name="Representation"></a>

There are five forms of character representation
1. _Intact form_ - matrix of _Dx_ by _Dy_.
2. _String form_ - one-dimensional array
   - along the X-axis - string of size _Dx_;
   - along the Y-axis - string of size _Dy_.
3. _Compact form_ - standalone character
   - _degenerate_ compact form - character of size _1×1_;
   - _normal_ compact form - character of size _Nx_ by _Ny_.
4. _Fraction form_ - autonomous fraction.
5. _Mixed form_ - combination of strings or/and fractions.

...the process of forming grapheme clusters is unambiguous, therefore, the terminal and the application do not have different interpretations of this functionality, unlike the interpretation of the width, therefore, the terminal must collect grapheme clusters

[ToC](#toc)

##### Packing <a name="Packing"></a>

... 7 bytes padding (assuming 64 bit architecture) ....

for every cell cwidth attribute calculated once or taken from global state and stored as attribute 

consider the case along one of the axes - _X-axis_

Scaling: 4 different states occupied 2 bits

|Parameter  |Field size|
|:----------|:--------:|
|char width | 2 bit    |

Fractioning: 9 different states occupied 4 bits:
 - 3 different sizes (size=1 is not fractionable)
   - size = 2
     - 2 different selectors
   - size = 3
     - 3 different selectors
   - size = 4
     - 4 different selectors
 
|Parameter  |Field size|
|:----------|:--------:|
|char width | 2 bit    |
|selector   | 2 bit    |

Scaling and Fractioning

|Parameter  |Field size|
|:----------|:--------:|
|char width | 2 bit    |
|selector   | 2 bit    |
|is_fract?  | 1 bit    | 

using the fact that the selector is always less than or equal to the width, and also that in the case of a unit width, scaling and fragmentation are the same

```
is_fract = selector<=width 
if not is_fract : swap(width, selector)
```

[ToC](#toc)

##### Normalization <a name="Normalization"></a>

```
...
```

[ToC](#toc)

##### Decomposition <a name="Decomposition"></a>

```
...
```

[ToC](#toc)

##### String View <a name="String"></a>

The string represents a sequence of the characters and it is the one-dimensional array by desing.
For example, consider a string contains one multi-sized (_2×2_) character in the middle
### <sub>string S = "uni </sub>S<sub>ize"</sub>

It can be represented in the one-dimensional array as following

Cooked for the screen buffer print output:

S[] = { {'u',Ps0}, {'n',Ps0}, {'i',Ps0}, {'S',`Ps1`}, {'S',`Ps2`}, {'i',Ps0}, {'z',Ps0}, {'e',Ps0} }

where
- Ps0 = _{ Nx=1, Dx=1, Ny=1, Dy=1 }_
- Ps1 = _{ Nx=1, Dx=2, Ny=2, Dy=1 }_
- Ps2 = _{ Nx=2, Dx=2, Ny=2, Dy=1 }_

It has screen length=8=sizeof(S), height=2=MAX(_Ny_(S)).
 
Follow you can see how the string S is printed in the sreen buffer at x=2,y=3

|     |  1  |  2  |  3  | ... | ... | ... | ... | ... | ... |  W  |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  1  | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
|  2  | ... | ... | ... | ... |S+Ps1|S+Ps2| ... | ... | ... | ... |
|  3  | ... |u+Ps0|n+Ps0|i+Ps0|S+Ps3|S+Ps4|i+Ps0|z+Ps0|e+Ps0| ... |
| ... | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |
|  H  | ... | ... | ... | ... | ... | ... | ... | ... | ... | ... |

where
- Ps0 = _{ Nx=1, Dx=1, Ny=1, Dy=1 }_
- Ps1 = _{ Nx=1, Dx=2, Ny=1, Dy=2 }_
- Ps2 = _{ Nx=2, Dx=2, Ny=1, Dy=2 }_
- Ps3 = _{ Nx=1, Dx=2, Ny=2, Dy=2 }_
- Ps4 = _{ Nx=2, Dx=2, Ny=2, Dy=2 }_

To store it as a unicode string it can be formatted as following

S[] = { {'u',Ps0}, {'n',Ps0}, {'i',Ps0}, {'S',`Ps1`}, {'i',Ps0}, {'z',Ps0}, {'e',Ps0} }

where
- Ps0 = _{ Nx=1, Dx=1, Ny=1, Dy=1 }_
- Ps1 = _{ Nx=2, Dx=1, Ny=2, Dy=1 }_

Note that the Ps0 can be omitted.  
`GCSCALE(Ps0)` = (1 - 1) + (1 - 1)×4 + (1 - 1)×16 + (1 - 1)×64 = 0  
`GCSCALE(Ps1)` = (2 - 1) + (1 - 1)×4 + (2 - 1)×16 + (1 - 1)×64 = `17`  

so  

VT-sequence example ...

~~if the GCSCALE unicode variation selectors block begins from \u0XXXX (GCSCALE1)  
`GCSCALE18` = \u0XXXX + 17 = GCSCALE1 + `17`~~

~~utf8text T = "uni"+'S'+<`GCSCALE18`>+"ize"~~

[ToC](#toc)

##### Scrollback Buffer <a name="Scrollback"></a>

array of strings

```
...
```

[ToC](#toc)

##### Screen Buffer / Plain Text File <a name="Screen"></a>

Each multisize character with a size of _n×m_ that is greater than _1×1_ is stored in the screen buffer (or monospaced text file) W×H as a matrix of _n×m_

Example:

The character _"A"_ of size _3×2_ with top left corner located at x = 3, y = 2 in the screen buffer (or monospaced text file)

|     |  1  |  2  |  3  | ... | ... | ... |  W  |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  1  | ... | ... | ... | ... | ... | ... | ... |
|  2  | ... | ... |A+Ps1|A+Ps2|A+Ps3| ... | ... |
| ... | ... | ... |A+Ps4|A+Ps5|A+Ps6| ... | ... |
| ... | ... | ... | ... | ... | ... | ... | ... |
|  H  | ... | ... | ... | ... | ... | ... | ... |

- A = _"A"_
- Ps1 = _{ Nx=1, Dx=3, Ny=1, Dy=2 }_
- Ps2 = _{ Nx=2, Dx=3, Ny=1, Dy=2 }_
- Ps3 = _{ Nx=3, Dx=3, Ny=1, Dy=2 }_
- Ps4 = _{ Nx=1, Dx=3, Ny=2, Dy=2 }_
- Ps5 = _{ Nx=2, Dx=3, Ny=2, Dy=2 }_
- Ps6 = _{ Nx=3, Dx=3, Ny=2, Dy=2 }_


_Ps_ can be packed in one byte and overhead of screen buffer is 1 byte per cell:

_byte = (Nx - 1) + (Dx - 1)×4 + (Ny - 1)×16 + (Dy - 1)×64_

~~Also there are only 256 variants for the Unicode modifier character value 0 - 255.~~

Characters with parameters _N > D_ are not allowed to be stored in a cell-based grid. When such a character is to be printed to the grid, it must be segmented for each grid cell, and the parameters are recalculated for each filled cell.

[ToC](#toc)

## 3 Expected Behavior <a name="Expected"></a>

It doesn’t matter what size (cwidth) the character has, it allows put a wide character to a single cell if you want.

No matter what size the character is, proportions should always be preserved.

```
...
```

[ToC](#toc)

#### 3.1 Printing <a name="Printing"></a>

Output examples (VT sequence <SCALE _Dx_ ; _Nx_ ; _Dy_ ; _Ny_>)
```
- cout “a” produce  1x1 in buffer:
  [1/1,1/1]
- cout “😊” produce 1x1 in buffer:
  [1/1,1/1]
- cout “👨‍👩‍👧‍👦” produce 1x1 in buffer: 
  [1/1,1/1]
- cout “<SCALE2;1;1;1>😀” produce 2x1, [1/2,1/1][2/2,1/1]
- cout “<SCALE3;1;3;1>😀” produce 3x3
- cout “<SCALE3;1;1;1>👨‍👩‍👧‍👦” produce 3x1 [1/3,1/1][2/3,1/1][3/3,1/1]
- cout “<SCALE1;1;1;1>😀X” produce 1x1, 1x1
- cout “<SCALE2;1;1;1>😀X<SCALE0;0;0;0>H” produce 2x1(😀), 2x1(X), 1x1(H)
- cout “<SCALE1;2;1;1>😀🌎XH😀😀” produce 
  [1/2,1/1](left half 😀), [2/2,1/1](right half 🌎), [1/2,1/1](left half X) , [2/2,1/1](right half H), [1/2,1/1](left half 😀) , [2/2,1/1](right half 😀)
```

It is also possible with this technique to print out mathematical expressions and multi-level formulas (*monospaced* plain text documents with formulas, CJK, wide emoji and so on.

[ToC](#toc)

###### Line Wrap <a name="Wrap"></a>

reverse-wraparound

how to decide whether an emoji (or a matching pair of two half-emojis) are allowed to be separated to different lines or not?

```
...
```

[ToC](#toc)

###### BiDi (LTR, RTL, TTB) <a name="BiDi"></a>
- memory representation
- display ordering
```
...
```

[ToC](#toc)

###### Side effects <a name="Side"></a>
```
...
```

[ToC](#toc)

### 3.2 Capturing <a name="Capturing"></a>
```
...
```

[ToC](#toc)

### 3.3 Collating <a name="Collating"></a>
```
...
```

[ToC](#toc)

### 3.4 Canonical Equivalence <a name="Canonical"></a>
```
...
```

[ToC](#toc)

## 4 Compatibility <a name="Compatibility"></a>

Applications and Existing Infrastructure

### 4.1 Terminal Emulators <a name="Emulators"></a>
```
...
```

[ToC](#toc)

### 4.2 Console Text Editors <a name="Editors"></a>
```
...
```

[ToC](#toc)

### 4.3 Terminal Multiplexers <a name="Multiplexers"></a>
```
...
```

[ToC](#toc)

### 4.4 POLA <a name="POLA"></a>

(?)

People are part of the system. The design should match the user's experience, expectations, and mental models.

```
...
```

[ToC](#toc)

## 5 Unicode Standard <a name="Unicode"></a>

At the moment, this option of marking is too difficult to implement, since there are no examples of successful application in the environment, therefore at the moment it is necessary to consider exclusively vt-sequences.

[ToC](#toc)

jloughry/Unicode, [How to make your Unicode proposal successful](https://github.com/jloughry/Unicode)

Name of the Unicode modifier letter (block 256)
  - \<VSSCALE>
  - \<GCSCALE1>..\<GCSCALE256> (like VS1..VS256)

Prefix or Suffix? <a name="PrefixSuffix"></a>

*Suffix* is absolutely immposible option,  
**Update:** Possible solution is using [BSU/ESU](https://gitlab.freedesktop.org/gnachman/specifications/-/blob/synchronized_updates/accepted/synchronized_updates.md)

because there is no way to restore the state of the screen/scrollback buffer (e.g. with a size equal to the size of the window or when a line is scrolled out) back after scrolling due to the arrival of a wide character, because it would not fit, and the narrow one would fit at the end of the current line, and the suffix modifier will appear later and require you to make a character narrow and requires rewinding scroll. This will completely lose the top/first line of the screen.

[ToC](#toc)

##### 5.1 Suffix <a name="Suffix"></a>

Latest Unicode Standard defines three types of variation sequences:
- Standardized variation sequences.
- Emoji variation sequences.
- Ideographic variation sequences defined in the Ideographic Variation Database.

Only those three types of variation sequences are sanctioned for use by conformant implementations.

[Accodingly to the Standardized variation sequences FAQ](http://unicode.org/faq/vs.html)

> Q: How can I propose a standardized variation sequence?

> A: You can initiate the process of requesting a variation sequence by submitting an inquiry via the contact form. A thorough understanding of how Variation Selectors are used will make a proposal more likely to be accepted by the UTC. Read Section 23.4, Variation Selectors, UTR #25 and UAX #34, as well as the rest of this FAQ for background information. [AF]

[Accodingly to the Section 23.4, Variation Selectors, UTR #25](http://www.unicode.org/versions/Unicode12.1.0/ch23.pdf#G19053)

> A variant form is a different glyph for a character, encoded in Unicode through the mechanism of variation sequences: sequences in Unicode that consist of a base character followed by a variation selector character.

[ToC](#toc)

##### Variation Sequence <a name="VS"></a>

> In a variation sequence the variation selector affects the appearance of the base character. Such changes in appearance may, in turn, have a visual impact on subsequent characters, particularly combining characters applied to that base character.

> The standardization or support of a particular variation sequence does not limit the set of glyphs that can be used to represent the base character alone. 

[ToC](#toc)

##### Placement in the Text <a name="Placement"></a>
```
<basechar><GCSCALE1..256>
```
In case of terminal application emits a unicode text without any Scale Modifiers ~~the terminal emulators should treat such text sequence as a sequence of the **narrow** grapheme clusters (narrow cwidth, _1×1_)~~, even CJK and friends. So the application CAN be responsible to define the width  (size: width & height) of each character it emits to the terminal.

Note about CJK  
There is no problem to fit the glyph into a rectangular cell of size _1×1_ preserving the proportions and increase terminal font size for readablity.

Example
- application emits a command to set global scale mode to _1×1_, all grapheme clusters are treated by term as _1×1_.
- application emits "Hi 😊!", the terminal should prints only 5 cells, all grapheme clusters are treated as _1×1_.
- application emits "Hi 😊\<GCSCALE2>!", the terminal should prints 6 cells - 😊 is treated as _2×1_.
- application emits "Hi 😊\<GCSCALE18>!", the terminal should prints 6 cells stright and 2 cell above (or under(?) RLF flag(?)) the smile - 😊 is treated as _2×2_.

[ToC](#toc)

##### 5.2 Prefix <a name="Prefix"></a>

slice p along X-axis <a name="sliceX"></a>
```
to take slice p of m 1xn from <basechar> of size m×n
<SLICE_MARK_p_1xn>
Dx=m Dy=1
Nx=p Ny=n

# P = Nx-1 + (Dx-1)×4 + (Ny-1)×16 + (Dy-1)×64

<SLICE_MARK_p_1xn><basechar>
```

slice q along Y-axis <a name="sliceY"></a>
```
to take slice q of n m×1 from <basechar> of size m×n
<SLICE_MARK_q_mx1>
Dx=1 Dy=n
Nx=m Ny=q

# P = Nx-1 + (Dx-1)×4 + (Ny-1)×16 + (Dy-1)×64

<SLICE_MARK_q_mx1><basechar>
```

fraction <a name="fraction"></a>
```
to take single fraction p×q from <basechar> of size m×n
<FRACT_MARK_pxq_nxm>
Dx=m Dy=n
Nx=p Ny=q

# P = Nx-1 + (Dx-1)×4 + (Ny-1)×16 + (Dy-1)×64

<FRACT_MARK_pxq_nxm><basechar>
```

character <a name="character"></a>
```
to scale standalone <basechar> from 1×1 up to 4×4
<SCALE_MARK_nxm>
Dx=Dy=1
Nx=m Ny=n

# P = Nx-1 + (Dx-1)×4 + (Ny-1)×16 + (Dy-1)×64

<SCALE_MARK_nxm><basechar>
```

embedding scaled text <a name="scaledtext"></a>
```
to embed scaled set of chars to size up to 4×4
<SCALE_FORMATTING_ABOVE_GLBL_mxn>
Nx=m and Dx=n and Ny=3 and Dy=2
(place characters above base line)
introduce global scale of size m by n formatting (global scope)

<SCALE_FORMATTING_UNDER_GLBL_mxn>
Nx=m and Dx=n and Ny=? and Dy=?
(place characters under base line)
introduce global scale of size m by n formatting (global scope)

<PUSH_SCALE_ABOVE_FORMATTING_PARA_mxn>
Nx=m and Dx=n and Ny=4 and Dy=2
(place characters above base line)
save (push to stack) current and introduce the scale of size m by n formatting (paragraph scope)

<PUSH_SCALE_UNDER_FORMATTING_PARA_mxn>
Nx=m and Dx=n and Ny=4 and Dy=3
(place characters under base line)
save (push to stack) current and introduce the scale of size m by n formatting (paragraph scope)

<RESERVED_SCALE_FORMATTING>


<POP_SCALE_FORMATTING_PARA>
Nx=4 and Dx=2 and Ny=1 and Dy=1
Ps = 7
restore (pop from stack) saved scaling

```
```
<SCALE_FORMATTING_ABOVE/UNDER_GLBL_mxn>
...mxn text...
<PUSH_SCALE_FORMATTING_ABOVE/UNDER_PARA_pxq> 
...pxq text... <SCALE/FRACT/SLICE_MARK_kxl><basechar>  ...pxq text... 
<POP_SCALE_FORMATTING_PARA>
...mxn text...
```

[ToC](#toc)

#### 5.3 Security Issues <a name="Security"></a>

##### Unicode Security Considerations <a name="Considerations"></a>

[Unicode Technical Report #36](http://unicode.org/reports/tr36/)

This section describes some of the security considerations that programmers, system analysts, standards developers, and users should take into account.

For example, consider visual spoofing, where a similarity in visual appearance fools a user and causes him or her to take unsafe actions.

```
...
```

[ToC](#toc)
